from django.shortcuts import *
from django.http import *
from .forms import *
from .links import *

def index(request):
  form = QuestionsForm(request.POST or None)
  if request.method == 'POST':
    if form.is_valid():
      context = {'site1':'link1',
                 'site2':'link2',
                 'site3':'link3'}

      return JsonResponse({'results':context})
  return render(request, 'ajax.html', {'form':form})
  



def index(request):
    if request.method == 'GET':
        form = QuestionsForm()
        return render(request, 'quazar/index.html', {'form':form})
    if request.method == 'POST':
        results = {}
        gender = request.POST.get('gender')
        age = request.POST.get('age')
        identity = request.POST.get('identity')
        reason_of_gifting = request.POST.get('reason_of_gifting')
        is_in_love = request.POST.get('is_in_love')
        hobby = request.POST.get('hobby')
        what_prefers = request.POST.get('what_prefers')
        recreation = request.POST.get('recreation')
        is_bookoholic = request.POST.get('is_bookoholic')
        is_shopoholic = request.POST.get('is_shopoholic')
        is_alcoholic = request.POST.get('is_alcoholic')
        planner = request.POST.get('planner')
        travel = request.POST.get('travel')
        car = request.POST.get('car')
        sugary = request.POST.get('sugary')
        jewelery = request.POST.get('jewelery')

        if gender == 'Чоловік':
            results.update(gender_man)
        if gender == 'Жінка':
            results.update(gender_woman)

        if age == 'Доросла':
            results.update(age_adult)
        if age == 'Дитина':
            results.update(age_kid)

        if identity == 'Cім\'я':
            results.update(identity_family)
        if identity == 'Знайомий':
            results.update(identity_familiar)
        if identity == 'Друг':
            results.update(identity_friend)
        if identity == 'Кохана людина':
            results.update(identity_loved)
        if identity == 'Коллега':
            results.update(identity_colleague)
        if identity == 'Інше':
            results.update(identity_other)

        if reason_of_gifting == 'День народження':
            results.update(birthday)
        if reason_of_gifting == 'Весілля':
            results.update(wedding)
        if reason_of_gifting == 'День закоханих':
            results.update(valentine_day)
        if reason_of_gifting == 'Випускний':
            results.update(outlet)
        if reason_of_gifting == 'Річниця':
            results.update(anniversary)
        if reason_of_gifting == 'Новий рік':
            results.update(new_year)
        if reason_of_gifting == 'Хрестини':
            results.update(christening)
        if reason_of_gifting == 'День захисника вітчизни':
            results.update(defender)
        if reason_of_gifting == 'День святого Миколая':
            results.update(saint_nikolas_day)
        if reason_of_gifting == 'Просто так':
            results.update(other_reason)

        if is_in_love == 'Так':
            results.update(is_in_love_yes)
        if is_in_love == 'Ні':
                results.update(is_in_love_no)
        if is_in_love == 'Не знаю':
                results.update(is_in_love_yes)
                results.update(is_in_love_no)

        if is_shopoholic == 'Так':
            results.update(is_shopoholic_yes)
        if is_shopoholic == 'Ні':
            results.update(is_shopoholic_no)
        if is_shopoholic == 'Не знаю':
            results.update(is_shopoholic_yes)
            results.update(is_shopoholic_no)

        if is_alcoholic == 'Так':
            results.update(is_alcoholic_yes)
        if is_alcoholic == 'Ні':
            results.update(is_alcoholic_no)
        if is_alcoholic == 'Не знаю':
            results.update(is_alcoholic_yes)
            results.update(is_alcoholic_no)

        if planner == 'Так':
            results.update(planner_yes)
        if planner == 'Ні':
            results.update(planner_no)
        if planner == 'Не знаю':
            results.update(planner_yes)
            results.update(planner_no)

        if travel == 'Так':
            results.update(travel_yes)
        if travel == 'Ні':
            results.update(travel_no)
        if travel == 'Не знаю':
            results.update(travel_yes)
            results.update(travel_no)

        if car == 'Так':
            results.update(car_yes)
        if car == 'Ні':
            results.update(car_no)
        if car == 'Не знаю':
            results.update(car_yes)
            results.update(car_no)

        if sugary == 'Так':
            results.update(sugary_yes)
        if sugary == 'Ні':
            results.update(sugary_no)
        if sugary == 'Не знаю':
            results.update(sugary_ye)
            results.update(sugary_no)

        if jewelery == 'Так':
            results.update(jewelery_yes)
        if jewelery == 'Ні':
            results.update(jewelery_no)
        if jewelery == 'Не знаю':
            results.update(jewelery_yes)
            results.update(jewelery_no)

        try:
            WHAT_PREFERS = [i for i in dict(request.POST).get('what_prefers')]
            RECREATION = [i for i in dict(request.POST).get('recreation')]
            IS_BOOKOHOLIC = [i for i in dict(request.POST).get('is_bookoholic')]
            HOBBY = [i for i in dict(request.POST).get('hobby')]

            if 'Оригінальним та незвичайним' in WHAT_PREFERS:
                results.update(WHAT_PREFERS_unusual)
            if 'Практичним та звичайним' in WHAT_PREFERS:
                results.update(WHAT_PREFERS_usual)

            if 'Активному' in RECREATION:
                results.update(RECREATION_active)
            if 'Пасивному' in RECREATION:
                results.update(RECREATION_passive)
            if 'Екстремальному' in RECREATION:
                results.update(RECREATION_extreme)

            if 'Художні' in IS_BOOKOHOLIC:
                results.update(IS_BOOKOHOLIC_fiction)
            if 'Науково-популярні' in IS_BOOKOHOLIC:
                results.update(IS_BOOKOHOLIC_popscience)
            if 'Для саморозвитку' in IS_BOOKOHOLIC:
                results.update(IS_BOOKOHOLIC_selfdevelopment)
            if 'Фантастика' in IS_BOOKOHOLIC:
                results.update(IS_BOOKOHOLIC_fantastic)
            if 'Для бізнесу' in IS_BOOKOHOLIC:
                results.update(IS_BOOKOHOLIC_busines)
            if 'Різне' in IS_BOOKOHOLIC:
                results.update(IS_BOOKOHOLIC_other)

            if 'К-поп' in HOBBY:
                results.update(HOBBY_kpop)
            if 'Спорт' in HOBBY:
                results.update(HOBBY_sport)
            if 'Кулінарія' in HOBBY:
                results.update(HOBBY_cooking)
            if 'Мистецтво' in HOBBY:
                results.update(HOBBY_art)
            if 'Шопінг' in HOBBY:
                results.update(HOBBY_shoping)
            if 'Аніме' in HOBBY:
                results.update(HOBBY_anime)
            if 'Технології' in HOBBY:
                results.update(HOBBY_tech)
            if 'Наука' in HOBBY:
                results.update(HOBBY_science)
            if 'Серіали' in HOBBY:
                results.update(HOBBY_series)
            if 'Кіно' in HOBBY:
                results.update(HOBBY_movies)
            if 'Відеоігри' in HOBBY:
                results.update(HOBBY_games)
            if 'Фотографія' in HOBBY:
                results.update(HOBBY_photo)
            if 'Риболовля' in HOBBY:
                results.update(HOBBY_fishing)
            if 'Бізнесу' in HOBBY:
                results.update(HOBBY_business)
            if 'Комікси/манга' in HOBBY:
                results.update(HOBBY_comics)
            if 'Мода і стиль' in HOBBY:
                results.update(HOBBY_fashion)
            if 'Музика' in HOBBY:
                results.update(HOBBY_music)
            if 'Інше' in HOBBY:
                results.update(HOBBY_other)
        except TypeError as e:
            print(e)
        except ValueError as v:
            print(v)
        return JsonResponse({
            'results':results
        })