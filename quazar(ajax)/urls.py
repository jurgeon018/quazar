from django.urls import path
from django.views.generic import TemplateView
from .views import *
from .forms import *

# app_name = 'quazar'

urlpatterns = [
    path('', index)
]
