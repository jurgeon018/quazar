from django.shortcuts import *
from django.http import *
from .forms import *
from .links import *
from .models import *


def index(request):
    form = QuestionsForm(request.POST or None)
    print(form.is_bound)
    if request.method == 'POST':
        print(form.is_bound, form.is_valid())
        results = {}
        gender = form.cleaned_data.get('gender')
        age = form.cleaned_data.get('age')
        identity = form.cleaned_data.get('identity')
        reason_of_gifting = form.cleaned_data.get('reason_of_gifting')
        is_in_love = form.cleaned_data.get('is_in_love')
        hobby = form.cleaned_data.get('hobby')
        what_prefers = form.cleaned_data.get('what_prefers')
        recreation = form.cleaned_data.get('recreation')
        is_bookoholic = form.cleaned_data.get('is_bookoholic')
        is_shopoholic = form.cleaned_data.get('is_shopoholic')
        is_alcoholic = form.cleaned_data.get('is_alcoholic')
        planner = form.cleaned_data.get('planner')
        travel = form.cleaned_data.get('travel')
        car = form.cleaned_data.get('car')
        sugary = form.cleaned_data.get('sugary')
        jewelery = form.cleaned_data.get('jewelery')

        if 'gender_man' in gender:
            results.update(gender_man)
        if 'gender_woman' in gender:
            results.update(gender_woman)

        if 'age_adult' in age:
            results.update(age_adult)
        if 'age_kid' in age  :
            results.update(age_kid)

        if 'identity_family' in identity  :
            results.update(identity_family)
        if 'identity_familiar' in identity  :
            results.update(identity_familiar)
        if 'identity_friend' in identity  :
            results.update(identity_friend)
        if 'identity_loved' in identity  :
            results.update(identity_loved)
        if 'identity_colleague' in identity  :
            results.update(identity_colleague)
        if 'identity_other' in identity  :
            results.update(identity_other)

        if 'birthday' in reason_of_gifting  :
            results.update(birthday)
        if 'wedding' in reason_of_gifting  :
            results.update(wedding)
        if 'valentine_day' in reason_of_gifting  :
            results.update(valentine_day)
        if 'outlet' in reason_of_gifting  :
            results.update(outlet)
        if 'anniversary' in reason_of_gifting  :
            results.update(anniversary)
        if 'new_year' in reason_of_gifting  :
            results.update(new_year)
        if 'christening' in reason_of_gifting  :
            results.update(christening)
        if 'defender' in reason_of_gifting  :
            results.update(defender)
        if 'saint_nikolas_day' in reason_of_gifting  :
            results.update(saint_nikolas_day)
        if 'other_reason' in reason_of_gifting :
            results.update(other_reason)

        if 'is_in_love_yes' in is_in_love  :
            results.update(is_in_love_yes)
        if 'is_in_love_no' in is_in_love  :
                results.update(is_in_love_no)
        if 'Не знаю' in is_in_love  :
                results.update(is_in_love_yes)
                results.update(is_in_love_no)

        if 'is_shopoholic_yes' in is_shopoholic  :
            results.update(is_shopoholic_yes)
        if 'is_shopoholic_no' in is_shopoholic  :
            results.update(is_shopoholic_no)
        if 'Не знаю' in is_shopoholic  :
            results.update(is_shopoholic_yes)
            results.update(is_shopoholic_no)

        if 'is_alcoholic_yes' in is_alcoholic  :
            results.update(is_alcoholic_yes)
        if 'is_alcoholic_no' in is_alcoholic  :
            results.update(is_alcoholic_no)
        if 'Не знаю' in  is_alcoholic :
            results.update(is_alcoholic_yes)
            results.update(is_alcoholic_no)

        if 'planner_yes' in planner  :
            results.update(planner_yes)
        if 'planner_no' in planner :
            results.update(planner_no)
        if 'Не знаю' in planner  :
            results.update(planner_yes)
            results.update(planner_no)

        if 'travel_yes' in travel  :
            results.update(travel_yes)
        if 'travel_no' in travel  :
            results.update(travel_no)
        if 'Не знаю' in travel  :
            results.update(travel_yes)
            results.update(travel_no)

        if 'car_yes' in car  :
            results.update(car_yes)
        if 'car_no' in car  :
            results.update(car_no)
        if 'Не знаю' in car  :
            results.update(car_yes)
            results.update(car_no)

        if 'sugary_yes' in sugary  :
            results.update(sugary_yes)
        if 'sugary_no' in sugary  :
            results.update(sugary_no)
        if 'Не знаю' in sugary  :
            results.update(sugary_yes)
            results.update(sugary_no)

        if 'jewelery_yes' in jewelery  :
            results.update(jewelery_yes)
        if 'jewelery_no' in jewelery:
            results.update(jewelery_no)
        if 'Не знаю' in jewelery:
            results.update(jewelery_yes)
            results.update(jewelery_no)

        if 'what_prefers_unusual' in what_prefers :
            results.update(what_prefers_unusual)
        if 'what_prefers_usual' in what_prefers  :
            results.update(what_prefers_usual)

        if 'recreation_active' in recreation:
            results.update(recreation_active)
        if 'recreation_passive' in recreation:
            results.update(recreation_passive)
        if 'recreation_extreme' in recreation:
            results.update(recreation_extreme)

        if 'is_bookoholic_fiction' in is_bookoholic  :
            results.update(is_bookoholic_fiction)
        if 'is_bookoholic_popscience' in is_bookoholic  :
            results.update(is_bookoholic_popscience)
        if 'is_bookoholic_selfdevelopment' in is_bookoholic  :
            results.update(is_bookoholic_selfdevelopment)
        if 'is_bookoholic_fantastic' in is_bookoholic  :
            results.update(is_bookoholic_fantastic)
        if 'is_bookoholic_busines' in is_bookoholic  :
            results.update(is_bookoholic_busines)
        if 'is_bookoholic_other' in is_bookoholic  :
            results.update(is_bookoholic_other)

        if 'hobby_kpop' in hobby:
            results.update(hobby_kpop)
        if 'hobby_sport' in hobby:
            results.update(hobby_sport)
        if 'hobby_cooking' in hobby:
            results.update(hobby_cooking)
        if 'hobby_art' in hobby:
            results.update(hobby_art)
        if 'hobby_shopping' in hobby:
            results.update(hobby_shopping)
        if 'hobby_anime' in hobby:
            results.update(hobby_anime)
        if 'hobby_tech' in hobby :
            results.update(hobby_tech)
        if 'hobby_science' in hobby:
            results.update(hobby_science)
        if 'hobby_series' in hobby :
            results.update(hobby_series)
        if 'hobby_movies' in hobby:
            results.update(hobby_movies)
        if 'hobby_games' in hobby:
            results.update(hobby_games)
        if 'hobby_photo' in hobby:
            results.update(hobby_photo)
        if 'hobby_fishing' in hobby:
            results.update(hobby_fishing)
        if 'hobby_business' in hobby :
            results.update(hobby_business)
        if 'hobby_comics' in hobby :
            results.update(hobby_comics)
        if 'hobby_fashion' in hobby  :
            results.update(hobby_fashion)
        if 'hobby_music' in hobby :
            results.update(hobby_music)
        if 'hobby_other' in hobby:
            results.update(hobby_other)
        print('s',form.cleaned_data)
        return JsonResponse({
            'results':results
        })
    return render(request, 'quazar/index.html', {'form':form})
