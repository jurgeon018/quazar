from django.db import models
from django.contrib import admin


class Category(models.Model):
    name = models.CharField(max_length=120)
    # links = models.ManyToManyField('Link')
    def __str__(self):
        return self.name


class Link(models.Model):
    name = models.CharField(max_length=100)
    link = models.TextField()
    categories = models.ManyToManyField('Category')
    def __str__(self):
        return self.name+'  ('+self.link+')'

from django.forms import widgets 
class LinkAdmin(admin.ModelAdmin):
    formfield_overrides = {

        models.ManyToManyField: {'widget': widgets.CheckboxSelectMultiple},
    }
admin.site.register(Link, LinkAdmin)
admin.site.register(Category)
