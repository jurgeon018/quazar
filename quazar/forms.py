from django import forms
from .choices import *

class QuestionsForm(forms.Form):
    gender = forms.ChoiceField(label='Якої статі ця людина?',
                               choices = genders_list)
    age = forms.ChoiceField(label='Якого віку ця людина?',
                            choices = ages_list)

    identity = forms.ChoiceField(label = 'Хто для тебе ця людина?',
                                choices = identity_list)

    reason_of_gifting = forms.ChoiceField(
                        label='З якої нагоди даруєш подарунок?',
                        choices=reason_of_gifting_list)

    is_in_love = forms.ChoiceField(label='Має кохану людину?',
                                   choices=is_in_love_list)

    is_shopoholic = forms.ChoiceField(label='Любить купувати цікаві речі для дому?',
                                      choices=shopo_list)

    is_alcoholic = forms.ChoiceField(label='Вживає алкоголь?',
                                     choices=alco_list)

    planner = forms.ChoiceField(label='Любить усе записувати і розплановувати?:',
                                choices=planner_list)

    travel = forms.ChoiceField(label='Подорожує?',
                               choices= travel_list)

    car = forms.ChoiceField(label='Має авто?',
                            choices = car_list)

    sugary = forms.ChoiceField(label='Любить солодке?',
                               choices=sugar_list)

    jewelery = forms.ChoiceField(
                  label='Часто носить різні види ювелірних прикрас?',
                  choices=jewelery_list)

    hobby = forms.MultipleChoiceField(
                label = 'Яке в людини хоббі?',
                choices=hobbies_list,
                widget = forms.CheckboxSelectMultiple,
                required=False)

    what_prefers = forms.MultipleChoiceField(
        label='Яким подарункам надає перевагу?',
        choices = what_prefers_list,
        widget=forms.CheckboxSelectMultiple,
        required=False)

    recreation = forms.MultipleChoiceField(
                        label='Якому відпочинку надає перевагу?',
                        choices=recreation_list,
                        widget=forms.CheckboxSelectMultiple,
                        required=False)

    is_bookoholic = forms.MultipleChoiceField(
            label='Які книги читає?',
            choices=booko_list,
            widget=forms.CheckboxSelectMultiple,
            required=False)
    class Meta:
        widgets = {
            'gender': forms.Select(attrs={'style':'float:left'}),
            'age': forms.Select(attrs={'style': 'float:left'}),
            'identity': forms.Select(attrs={'style':'float:left'}),
            'reason_of_gifting': forms.Select(attrs={'style':'float:left'}),
            'is_in_love': forms.Select(attrs={'style':'float:left'}),
        }


    # content = forms.CharField(widget=forms.Textarea(attrs={
    #                                 'class': 'form-control',
    #                                 'id': 'id_content',
    #                                 'placeholder': 'Type your comment',
    #                                 'rows': '4'
    # })) # создаст это <div id="<textarea name="usercomment" id="usercomment" placeholder="Type your comment" class="form-control"></textarea>"></div>

