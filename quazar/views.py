from django.shortcuts import *
from django.http import *
from .forms import *
from .links import *
from .models import *


def index(request):
    if request.method == 'GET':
        form = QuestionsForm()
        return render(request, 'quazar/index.html', {'form': form})
    if request.method == 'POST':
        form = QuestionsForm(request.POST)
        if form.is_valid():
            gender = form.cleaned_data.get('gender')
            age = form.cleaned_data.get('age')
            identity = form.cleaned_data.get('identity')
            reason_of_gifting = form.cleaned_data.get('reason_of_gifting')
            is_in_love = form.cleaned_data.get('is_in_love')
            hobby = form.cleaned_data.get('hobby')
            what_prefers = form.cleaned_data.get('what_prefers')
            recreation = form.cleaned_data.get('recreation')
            is_bookoholic = form.cleaned_data.get('is_bookoholic')
            is_shopoholic = form.cleaned_data.get('is_shopoholic')
            is_alcoholic = form.cleaned_data.get('is_alcoholic')
            planner = form.cleaned_data.get('planner')
            travel = form.cleaned_data.get('travel')
            car = form.cleaned_data.get('car')
            sugary = form.cleaned_data.get('sugary')
            jewelery = form.cleaned_data.get('jewelery')
            categories = [gender,
                          age,
                          identity,
                          reason_of_gifting,
                          is_in_love,
                          hobby,
                          what_prefers,
                          recreation,
                          is_bookoholic,
                          is_shopoholic,
                          is_alcoholic,
                          planner,
                          travel,
                          car,
                          sugary,
                          jewelery,

                          ]
            results = {}
            for link_ in Link.objects.all():
                # print('link_: ',link_)
                for category in link_.categories.all():
                    # print('type(category): ', type(category))
                    # print('category.name: ', category.name)
                    # print('"age_kid" == category: ', 'age_kid' == category.name)
                    # print('"age_kid" in categories: ', 'age_kid' in categories)
                    # print('category in categories: ', category.name in categories)
                    if category.name in categories:
                        results.update({link_.name: link_.link})
                        # print('results: ',results)

            return JsonResponse({
                'results': results
            })
