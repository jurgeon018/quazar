genders_list = (
  ('gender_man', 'Чоловік'),
  ('gender_woman', 'Жінка')
)

ages_list = (
  ('age_kid','Дитина'),
  ('age_adult','Доросла')
)

identity_list = (
  ('identity_family','Cім\'я'), ('identity_familiar','Знайомий'),('identity_friend','Друг'),
  ("identity_loved","Кохана людина"), ("identity_colleague","Колега"), ("identity_other","Інший")
)

reason_of_gifting_list = (
  ('birthday','День народження'),
  ('wedding','Весілля'), 
  ('valentine_day','День закоханих'),
  ("anniversary","Річниця"), 
  ('defender','День захисника вітчизни'),
  ("outlet","Випускний"), 
  ('saint_nikolas_day','День святого Миколая'),
  ('new_year','Новий рік'), 
  ('christening','Хрестини'), 
  ('other_reason','Просто так')
)

is_in_love_list = (
  ('is_in_love_yes','Так'),
  ('is_in_love_no','Ні'),
  ('Не знаю','Не знаю')
)

hobbies_list = (
  ('hobby_sport','Спорт'),
  ('hobby_cooking','Кулінарія'),
  ('hobby_business','Бізнес'), 
  ('hobby_fishing','Риболовля'),
  ('hobby_art','Мистецтво'), 
  ('hobby_shopping','Шопінг'),
  ('hobby_anime','Аніме'), 
  ('hobby_kpop','К-поп'), 
  ('hobby_tech','Технології'), 
  ('hobby_science','Наука'), 
  ('hobby_games','Відеоігри'), 
  ('hobby_photo','Фотографія'),
  ('hobby_movies','Кіно'), 
  ('hobby_series','Серіали'), 
  ('hobby_comics','Комікси/манга'),
  ('hobby_fashion','Мода і стиль'), ('hobby_music','Музика'),
  ('hobby_other','Інше хоббі'),
  ('Не знаю','Не знаю')
)

what_prefers_list = (
  ('what_prefers_unusual','Оригінальним та незвичайним'),
  ('what_prefers_usual', 'Практичним та звичайним')
)

recreation_list = (
  ('recreation_active','Активному'), ('recreation_passive','Пасивному'), ('recreation_extreme','Екстремальному')
)

booko_list = (
  ('Ніякі','Ніякі'),
  ('is_bookoholic_fiction','Художні'), 
  ('is_bookoholic_popscience','Науково-популярні'), ('is_bookoholic_selfdevelopment','Для саморозвинту'), 
  ('is_bookoholic_fantastic','Фантастику'), 
  ('is_bookoholic_busines','Для бізнесу'), 
  ('is_bookoholic_other','Різне')
)

shopo_list = (
  ('is_shopoholic_yes','Так'),('is_shopoholic_no','Ні'),
  ('Не знаю','Не знаю')
)

alco_list = (
  ('is_alcoholic_yes','Так'),
  ('is_alcoholic_no','Ні'),
  ('Не знаю','Не знаю')
)

planner_list = (
  ('planner_yes','Так'),
  ('planner_no','Ні'),
  ('Не знаю','Не знаю')
)

travel_list = (
  ('travel_yes','Так'),
  ('travel_no','Ні'),
  ('Не знаю','Не знаю')
)

car_list = (
  ('car_yes','Так'),
  ('car_no','Ні'),
  ('Не знаю','Не знаю')
)

sugar_list = (
  ('sugary_yes','Так'), 
  ("sugary_no","Ні"), 
  ("Не знаю","Не знаю")
)

jewelery_list = (
  ('jewelery_yes','Так'),
  ('jewelery_no','Ні'),
  ('Не знаю','Не знаю')
)
