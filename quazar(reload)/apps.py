from django.apps import AppConfig


class QuazarConfig(AppConfig):
    name = 'quazar'
