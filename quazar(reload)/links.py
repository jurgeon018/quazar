gender_man = {
'Набір чоловічої косметики':'https://makeup.com.ua/categorys/357011',
'Парфумерія для чоловіків': 'https://makeup.com.ua/categorys/3218/?utm_source=gifthub&utm_medium=cpc',
}

gender_woman = {
'Бомбочки для ванни': 'https://makeup.com.ua/search/?q=%D0%B1%D0%BE%D0%BC%D0%B1%D0%BE%D1%87%D0%BA%D0%B8+%D0%B4%D0%BB%D1%8F+%D0%B2%D0%B0%D0%BD%D0%BD%D1%8B?utm_source=gifthub&utm_medium=cpc',
'Набір чоловічої косметики': 'https://makeup.com.ua/categorys/357011/?utm_source=gifthub&utm_medium=cpc',
'Ароматична вода і спрей для лиця': 'https://makeup.com.ua/categorys/473799/?utm_source=gifthub&utm_medium=cpc',
'Аромати для дому':'https://makeup.com.ua/categorys/521401/?utm_source=gifthub&utm_medium=cpc',
'Косметика для ванної та душу':'https://makeup.com.ua/categorys/22822/?utm_source=gifthub&utm_medium=cpc',
'Парфумерія для жінок':'https://makeup.com.ua/categorys/324237/?utm_source=gifthub&utm_medium=cpc',
'Набори інструментів для манікюру та педикюру':'https://makeup.com.ua/categorys/295601/?utm_source=gifthub&utm_medium=cpc',
'Косметичні футляри':'https://makeup.com.ua/categorys/23465/?utm_source=gifthub&utm_medium=cpc',
'Ароматичні свічки':'https://makeup.com.ua/categorys/393287/?utm_source=gifthub&utm_medium=cpc',
'Подарункові сертифікати від MakeUp':'https://makeup.com.ua/categorys/58881/?utm_source=gifthub&utm_medium=cpc',
'Засоби для догляду за обличчям і тілом':'https://makeup.com.ua/categorys/431931/?utm_source=gifthub&utm_medium=cpc',
'Набори декоративної косметики':'https://makeup.com.ua/categorys/60537/?utm_source=gifthub&utm_medium=cpc',
'Парфумерія для чоловіків':'https://makeup.com.ua/categorys/3218/?utm_source=gifthub&utm_medium=cpc',
'Аромати для дому':'https://makeup.com.ua/categorys/521401/?utm_source=gifthub&utm_medium=cpc',
'Аксесуари для макіяжу':'https://makeup.com.ua/categorys/340641/?utm_source=gifthub&utm_medium=cpc',
'Інструменти та аксесуари для догляду за волоссям':'https://makeup.com.ua/categorys/490153/?utm_source=gifthub&utm_medium=cpc',
'Натуральна та органічна косметика':'https://makeup.com.ua/categorys/34957/?utm_source=gifthub&utm_medium=cpc',
'Щітка для волосся':'https://makeup.com.ua/categorys/340645/?utm_source=gifthub&utm_medium=cpc'
}

age_adult = {

}
age_kid = {
'Гра Playstation в VR окулярах для двох': 'https://www.bodo.ua/go/playstation-v-vr-ochkakh/?utm_source=gifthub&utm_medium=cpc&utm_campaign=playstation-v-vr-ochkakh',
'Наукові ігри': 'https://www.ranok.com.ua/cat/naukovi-igri-493.html',
'Плакат для дитини «Мій перший рік»': 'https://www.yakaboo.ua/ua/plakat-moj-pervyj-god.html?utm_source=sellaction.net&SAuid=62fe960fae196fdc&utm_campaign=100027213&utm_content=14726',
'Сертифікат в дитячу лабораторію експериментів': 'https://www.bodo.ua/go/detskaya-laboratoriya-eksperimentov/?utm_source=gifthub&utm_medium=cpc&utm_campaign=detskaya-laboratoriya-eksperimentov',
'Скретч-постер  «100 справ для дитини»':
'https://www.yakaboo.ua/ua/skretch-poster-1dea-me-100-sprav-junior-edition-100jua.html?utm_source=sellaction.net&SAuid=62fe960fae196fd3&utm_campaign=100027213&utm_content=14726',
'Набір дитячих шкарпеток': 'https://griffonsocks.com.ua/ru/product-category/kids/',
'Дитяча література': 'https://www.yakaboo.ua/knigi/detskaja-literatura.html?utm_source=sellaction.net&SAuid=62fe960faf1467da&utm_campaign=100027213&utm_content=14726',
'Скетч-ростомір для дитини': 'https://www.yakaboo.ua/ua/skretch-rostomer-1dea-me-volshebnye-prikljuchenija.html?utm_source=sellaction.net&SAuid=62fe960faf1864dd&utm_campaign=100027213&utm_content=14726'
}

identity_family = {}
identity_familiar = {}
identity_friend = {}
identity_loved = {}
identity_colleague = {}
identity_other = {}

birthday = {}
wedding = {}
valentine_day = {}
outlet = {}
anniversary = {}
new_year = {}
christening = {}
defender = {}
saint_nikolas_day = {}
other_reason = {}

is_in_love_yes = {}
is_in_love_no= {}

is_shopoholic_yes = {}
is_shopoholic_no = {}

is_alcoholic_yes = {
'Вино':
'https://okwine.ua/vina.html?utm_source=GiftHubBot&utm_medium=telegram&utm_campaign=GiftHub&utm_term=vina',
'Міцний алкоголь':
'https://okwine.ua/vina.html?utm_source=GiftHubBot&utm_medium=telegram&utm_campaign=GiftHub&utm_term=vina',
'Лікери':
'https://okwine.ua/liker.html?utm_source=GiftHubBot&utm_medium=telegram&utm_campaign=GiftHub&utm_term=liker',
'Ігристе вино':
'https://okwine.ua/igristoe-vino.html?utm_source=GiftHubBot&utm_medium=telegram&utm_campaign=GiftHub&utm_term=igristoe-vino',
'Міцний алкоголь':
'https://okwine.ua/krepkij-alkogol.html?utm_source=GiftHubBot&utm_medium=telegram&utm_campaign=GiftHub&utm_term=krepkij-alkogol',
}
is_alcoholic_no = {}

planner_yes = {}
planner_no = {}

travel_yes = {}
travel_no = {}

car_yes = {}
car_no = {}

jewelery_yes = {}
jewelery_no = {}

sugary_yes = {}
sugary_no = {}


WHAT_PREFERS_unusual = {
'Екокуб':
'https://superpupers.com/328-kategorii/1166-kreativnyy-interer/zhivoj-ugolok/nabori-dlja-virashhivanija-/jekokubi/',
'Книга сейф':
'https://superpupers.com/328-kategorii/1166-kreativnyy-interer/hranenie/1220-knigi-seyfy/',
'Оригінальна подушка':
'https://superpupers.com/328-kategorii/1166-kreativnyy-interer/tekstil/dekorativnie-podushki/',
'Магнітна дошка':
'https://superpupers.com/328-kategorii/1166-kreativnyy-interer/svet-i-ujut/459-magnitnye-doski',
'Постери':
'https://superpupers.com/328-kategorii/1166-kreativnyy-interer/nastennij-dekor/1795-krutye-postery/?utm_source=gifthub&utm_medium=telegram&utm_campaign=gifthubposter',
'Тарілочка добра':
'https://superpupers.com/328-kategorii/1166-kreativnyy-interer/kuhnja/1969-tarelochki-dobra/?utm_source=gifthub&utm_medium=telegram&utm_campaign=gifthubtareliki'
}
WHAT_PREFERS_usual = {}

RECREATION_active = {}
RECREATION_passive = {}
RECREATION_extreme = {}


IS_BOOKOHOLIC_fiction = {}
IS_BOOKOHOLIC_popscience = {}
IS_BOOKOHOLIC_selfdevelopment= {}
IS_BOOKOHOLIC_fantastic = {}
IS_BOOKOHOLIC_busines = {}
IS_BOOKOHOLIC_other = {}

HOBBY_kpop = {
'Деревянний блокнот ЕХО':
'http://k-popla.net/products/derevyannyj-k-pop-bloknot-exo',
'Сумка-бананка WANNA ONE':
'http://k-popla.net/products/sumka-bananka-wanna-one---holographic',
'К-рор кольцо Гірлс Генератіон':
'http://k-popla.net/products/-k-pop-koltso-girls-generation',
'к-рор кольцо супер джуніор':
'http://k-popla.net/products/k-pop-koltso-super-junior-',
'к-рор кольцо бтс':
'http://k-popla.net/products/k-pop-koltso-bts---type',
'брелки':
'http://k-popla.net/products/brelki-bt21btstwiceseventeen',
'футболка бтс':
'http://k-popla.net/products/futbolka-bts-youth-league---jin',
'Каблучка bigbang':
'http://k-popla.net/products/koltso-bigbang---vip',
'Брелок BT21/BTS/TWICE/SEVENTEEN':
'http://k-popla.net/products/brelki-bt21btstwiceseventeen',
'Сумка для взуття BTS':
'http://k-popla.net/products/sumka-meshok-dlya-obuvi-bts-',
'Дерев\'яний блокнот BTS':
'http://k-popla.net/products/derevyannyj-k-pop-bloknot-bts---army',
'Кепка Seventeen':
'http://k-popla.net/products/k-pop-kepka-seventeen',
'Сумка-бананка Monsta X':
'http://k-popla.net/products/sumka-bananka-monsta-x---holographic',
'Магнітні закладинки Seventeen/Twice':
'http://k-popla.net/products/magnitnye-zakladki-seventeentwice',
'Кільце-світлячок BTS':
'http://k-popla.net/products/koltso-svetlyachok-bts',
'Сумка для взуття EXO':
'http://k-popla.net/products/sumka-meshok-dlya-obuvi-exo',
'Каблучка біг банг':
'http://k-popla.net/products/koltso-bigbang---vip',
}
HOBBY_sport = {}
HOBBY_cooking = {}
HOBBY_art = {}
HOBBY_shoping = {}
HOBBY_anime = {}
HOBBY_tech = {}
HOBBY_science = {}
HOBBY_series = {}
HOBBY_movies = {}
HOBBY_games = {}
HOBBY_photo = {}
HOBBY_fishing = {}
HOBBY_business = {}
HOBBY_comics = {
'Комікси': 'https://cosmic.com.ua/comics/?utm_source=gifthub',
'Артбук Стен Лі': 'https://cosmic.com.ua/sten_li_sozdatel_velikoy_vselennoy_marvel_biografiya/?utm_source=gifthub',
'Іграшки та фігурки героїв': 'https://cosmic.com.ua/toys/?utm_source=gifthub',
'Подарунковий сертифікат на покупку коміксів':'https://cosmic.com.ua/gift-cards/?utm_source=gifthub',
'Підстаканник з символікою коміксів':'https://cosmic.com.ua/coasters/?utm_source=gifthub',
'Артбуки':'https://cosmic.com.ua/artbook/?utm_source=gifthub',
'Іграшки та фігурки героїв':'https://cosmic.com.ua/toys/?utm_source=gifthub',
'Книга про "Гру Престолів"':'https://cosmic.com.ua/game_of_thrones_blagorodnye_doma_vesterosa/?utm_source=gifthub',
'Офіційні шарфи DC':'https://cosmic.com.ua/scarves/?utm_source=gifthub',
'Брелки з символікою коміксів та фільмів':'https://cosmic.com.ua/key-rings/?utm_source=gifthub',
'Офіційний випуск серіалу "Breaking Bad"':'https://cosmic.com.ua/breaking_bad_ofitsialnoe_izdanie_seriala_vo_vse_tyazhkie/?utm_source=gifthub',
}
HOBBY_fashion = {}
HOBBY_sport = {}
HOBBY_music = {}
HOBBY_other = {}
