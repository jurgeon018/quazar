from django.db import models
from django.contrib import admin


class Links(models.Model):
    category = models.CharField(max_length=100)
    link = models.TextField()
    

admin.site.register(Links)
