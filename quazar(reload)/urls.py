from django.urls import path
from django.views.generic import TemplateView
from .views import *
from .forms import *

# app_name = 'quazar'

urlpatterns = [
    path('', TemplateView.as_view(template_name='quazar/index.html',
                                  extra_context={}), name='quazar_index'),
    path('main/', TemplateView.as_view(template_name='quazar/main.html',
                                       extra_context={'form':QuestionsForm()})),
    path('results/', results, name='results')
]
